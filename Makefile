## 定义基本编译命令到变量中
# CC=gcc
# CXX=g++

default:
	cmake -S . -B build -GNinja 

release: 
	cmake -S . -B build -GNinja -DCMAKE_BUILD_TYPE=Release
	cmake --build build

debug:
	cmake -S . -B build -GNinja -DCMAKE_BUILD_TYPE=Debug
	cmake --build build

docs: 
	cmake --build build --target docs

test:
	cmake --build build --target test

clean:
	rm build -rf

.PHONY: help clean

help:
	@echo "Usage: make [target]"
	@echo "  default: build the project"
	@echo "  release: build the project with release mode"
	@echo "  debug: build the project with debug mode"
	@echo "  docs: build the project documentation"
	@echo "  clean: clean the project"
	@echo "  help: show this help message"
